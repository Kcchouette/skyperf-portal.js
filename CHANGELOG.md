# CHANGELOG

## v0.2.x to 0.3.0

### API Breaking changes

 - Rename `getExecuteReloadingExpression(skynetElement)` → `getExecuteAfterReloadingExpression(skynetElement)`

## v0.1.x to v0.2.0

### API Breaking changes

 - Rename `SkyPerfPortal.linkRenew` → `SkyPerfPortal.generateAgain`