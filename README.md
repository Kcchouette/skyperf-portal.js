# SkyPerf-Portal

JS Lib that choose the BEST PORTAL for YOUR need and at YOUR time!<br>

## To Sum up

SkyPerf-Portal.js is a library that developers can add to their applications. This library looks at all of the public portals and selects the one that is performing the best at the moment for that particular user.<br>
Because it is done client-side, it will be able to identify different portals correctly for users in different regions.<br>
SkyPerf makes lives easier for developers while also improving the user experience of applications across the ecosystem.

## Use cases

 - Use any public skynet portal
 - Benefit of federation of portal
 - Use a not-to-much-load portal very easily without changing URL of your website
 - Extensible (but you need to know JavaScript)

## Process

1. Upload your video / sub / whatever on siasky.net or another portal
1. Put our class on element to permit us to treat
1. Change the source attribute of the element to our own
1. That's all

## Public and Documented API

### Initialize and use SkyPerf-Portal

1. Write your file in good HTML5 element
2. Integrate our already-done class:
    - `class="spp-videos"` inside `<video>` tag
    - `class="spp-audios"` inside `<audio>` tag
    - `class="spp-links"` inside `<a>` tag
    - `class="spp-js-external"` inside `<script>` tag
    - `class="spp-css-external"` inside `<link>` tag
    - `class="spp-imgs"` inside `<img>` tag
    - Your own... See [below](#Use-your-own-Class-rewritting)
3. Change the the source attribute of the element to our own:
    - `spp-link="<hash of skylink>` instead of `src="https://<portal>/<hash of skylink>` for `<source>`/`<track>` child tag of `<video>`/`<audio>` tag
    - `spp-link="<hash of skylink>` instead of `href="https://<portal>/<hash of skylink>` for `<a>` tag
    - ...
    - to generalize: For an element A, put `<getSPPClassLink() of (child) element>="<hash of skylink>` instead of `<getSPPToAttribute of (child) element>="https://<portal>/<hash of skylink>` for `<getCompatibleTag()>` (child) tag; (child) is decided by `getType()`
4. Add our script 
```
<script src="dist/skyperf-portal.min.js"></script>
```
5. Call the `initialize()` function
```
<script>
  SkyPerfPortal.initialize();
</script>
```
6. Done o/

### Generate source / link Again

You can generate new portal link/source
 - by reloading the page,
 - or, for example by adding a button 
 ```
 <button onclick="SkyPerfPortal.generateAgain(idOfElementToGenerateAgain, new Link, false)">Refresh video link</button>
 <button onclick="SkyPerfPortal.generateAgain(idOfElementToGenerateAgain, new Link, true)">Alternate video link</button> 
 ```

As you can imagine, `generateAgain()` can have up to 3 args:
 - `idOfElementToGenerateAgain`, the html id of the element to generate the source / link again
 - `new Link`, the class of the element; it can be
    - `new VideoHTML5`, for `<video>` tag
    - `new AudioHTML5`, for `<audio>` tag
    - `new Link`, for `<a>` tag
    - `new JSExternalFile`, for `<script>` tag
    - `new CSSExternalFile`, for `<link>` tag
    - your own :) See [below](#use-your-own-class-rewritting)
 - `true` or `false`; it'll blacklist the portail temporary to permit to use another portal in case the value is `true` (default is `false`)

### Blacklist Portal

Be careful that blacklisting many portals may result to have elements not-loading / more timeout (see [below](#changing-default-tiemout)).

```
blacklist.addToBlacklist("https://[...].com");
blacklist.addToBlacklist("https://siasky.net");
```

### Changing default timeout

The default timeout has been put to 2500 ms. You can increase or decrease this value

The pro: if you increase this value, it'll permit to blacklist more portals, and elements will load anyway.

```
timeout = <duration_in_milliseconds>;
```

### Use your own Class rewritting

On the `js` part of your HTML, add this kind of class, that `extends StandardElement`
```
class Img extends StandardElement {
    getType() {
        return Type.Parent;
    }
    getCSSClass() {
        return "img.spp-imgs";
    }
    getCompatibleTag() {
        return ["IMG"];
    }
    getSPPToAttribute() {
        return "src";
    }
}
```

 - `getType()`: can be `Type.Parent` or `Type.Child`; it indicates where the process will occur; for eg, on img in our case, it's `Type.Parent` because in the tag `<img` the class `class="spp-imgs"`, the `spp-links` and the `src` are on the same level.<br>
 In the case of `video`, the class `class="spp-videos"` is located in parent, and the `src` and the `spp-links` in child.
 - `getCSSClass()`: is the class on which elements the process will occur; to be more precise we'll use `tag.class`
 - `getCompatibleTag()`: it's to limit elements, more useful in case of `Type.Child`, as for example `<audio>` can have more tags than `<source>` (the `<a>` tag, where we will apply the `Link` process instead).  The content is on UPPERCASE.
 - `getSPPToAttribute()`: it's the attribute where we will put the skynet-portal link of the element coming from `getCompatibleTag()`
 - `getExecuteBeforeReloadingExpression(skynetElement)` / `getExecuteAfterReloadingExpression(skynetElement)`: `skynetElement` can be the element (in case of `Type.Parent`) or the child (in case of `Type.Child`).<br>
 It has been useful in case of `<audio>` and `<video>`, to reload element after changing the `src`.
 - `getSPPClassLink()`: the location of the skynet hash, per default for all the element `spp-links`

Then add it to our internal list:
```
SkyPerfPortal.classToPush(new Img);
```

To finish, you can initialize our library:
```
SkyPerfPortal.initialize();
```

## TODO / Ideas

 - None for the moment.

## Release

Create the `min.js` file using [terser](https://github.com/terser/terser#install):

```
$ terser --compress --mangle -- skyperf-portal.js > dist/skyperf-portal.min.js
```

## FAQ

### How long are files available for?

Files are available to be downloaded for a undefinied time (3 months per default).
It depends on portal configuration about file storage on their sia daemon.
We can't guarantee that a file will be gone after a week.

## Disclaimer

SkyPerf-Portal is an experiment and under active development. The answers here may change as we get feedback from you and the project matures.