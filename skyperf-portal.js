let timeout = 2500;

/**
 * @class Blacklist
 */
class Blacklist {
    /**
     * Initialize to empty the portalBlacklist array
     * @memberof Blacklist
     */
    constructor() {
        this.portalBlacklist = [];
    }

    /**
     * @returns the portalBlacklist array
     * @memberof Blacklist
     */
    getBlacklist() {
        return this.portalBlacklist;
    }

    /**
     * Add a portalUrl and check it's not already included
     * @param {string} portalUrl a portal URL
     * @returns the portalBlacklist array
     * @memberof Blacklist
     */
    addToBlacklist(portalUrl) {
        if (!this.includePortal(portalUrl))
            this.portalBlacklist.push(portalUrl);
    }

    /**
     * Empty the portalBlacklist array
     * @memberof Blacklist
     */
    resetBlacklist() {
        this.portalBlacklist = [];
    }

    /**
     * Check that a given portal URL is not in the portalBlackList Array
     * @param {string} portalUrl a portal URL
     * @returns {boolean}
     * @memberof Blacklist
     */
    includePortal(portalUrl) {
        return this.portalBlacklist.includes(portalUrl)
    }
}
let blacklist = new Blacklist;

/**
 * The Type of Element to override
 */
const Type = {
    Parent: 1,
    Child: 2,
};

/**
 * @class StandardElement
 */
class StandardElement {

    /**
     * @returns {Type} the Type of Element to override
     * @memberof StandardElement
     */
    getType() {
        return null;
    }

    /**
     * @returns {string} The location of the skynet hash
     * @memberof StandardElement
     */
    getSPPClassLink() {
        return "spp-link";
    }

    /**
     * @returns {string} The class on which elements the process will occur
     * @memberof StandardElement
     */
    getCSSClass() {
        return "";
    }

    /**
     * Limit elements, more useful in case of `Type.Child`
     * should be in UPPER case
     * @returns {Array}
     * @memberof StandardElement
     */
    getCompatibleTag() {
        return [];
    }

    /**
     * the attribute where we will put the skynet-portal link of the element
     * @returns {string}
     * @memberof StandardElement
     */
    getSPPToAttribute() {
        return "";
    }

    /**
     * Function to execute some operation before changing source
     * @memberof StandardElement
     */
    getExecuteBeforeReloadingExpression() {
        void(0);
    }

    /**
     * Function to execute some operation after changing source
     * @memberof StandardElement
     */
    getExecuteAfterReloadingExpression() {
        void(0);
    }
}

class VideoHTML5 extends StandardElement {

    getType() {
        return Type.Child;
    }

    getCSSClass() {
        return "video.spp-videos";
    }

    getCompatibleTag() {
        return ["SOURCE", "TRACK"];
    }

    getSPPToAttribute() {
        return "src";
    }

    getExecuteAfterReloadingExpression(skynetElement) {
        const reloading = new Function("skynetElement", "skynetElement.parentElement.load();");
        reloading(skynetElement);
    }
}

class AudioHTML5 extends StandardElement {

    getType() {
        return Type.Child;
    }

    getCSSClass() {
        return "audio.spp-audios";
    }

    getCompatibleTag() {
        return ["SOURCE"];
    }

    getSPPToAttribute() {
        return "src";
    }

    getExecuteAfterReloadingExpression(skynetElement) {
        let reloading = new Function("skynetElement", "skynetElement.parentElement.load();");
        reloading(skynetElement);
    }
}

class Link extends StandardElement {

    getType() {
        return Type.Parent;
    }

    getCSSClass() {
        return "a.spp-links";
    }

    getCompatibleTag() {
        return ["A"];
    }

    getSPPToAttribute() {
        return "href";
    }
}

class Img extends StandardElement {
    getType() {
        return Type.Parent;
    }
    getCSSClass() {
        return "img.spp-imgs";
    }
    getCompatibleTag() {
        return ["IMG"];
    }
    getSPPToAttribute() {
        return "src";
    }
}

class JSExternalFile extends StandardElement {

    getType() {
        return Type.Parent;
    }

    getCSSClass() {
        return "script.spp-js-external";
    }

    getCompatibleTag() {
        return ["script"];
    }

    getSPPToAttribute() {
        return "src";
    }
}

class CSSExternalFile extends StandardElement {

    getType() {
        return Type.Parent;
    }

    getCSSClass() {
        return "link.spp-css-external";
    }

    getCompatibleTag() {
        return ["link"];
    }

    getSPPToAttribute() {
        return "href";
    }
}

/**
 * Class above to use, and be modified with
 * SkyPerfPortal.classToPush(classtoPush);
 */
let elementsToUse = [new VideoHTML5, new AudioHTML5, new Link, new Img, new JSExternalFile, new CSSExternalFile];

class SkyPerfPortal {

    constructor() {}

    /**
     * Initialize!
     * @static
     * @memberof SkyPerfPortal
     */
    static initialize() {
        this.skyPerfPortal = new SkyPerfPortal;
        for (let i = 0; i < elementsToUse.length; i++) {
            let elementToUse = elementsToUse[i];
            let skynetElementInHTML = document.querySelectorAll(elementToUse.getCSSClass());
            for (let j = 0; j < skynetElementInHTML.length; j++) {
                this.skyPerfPortal.prepareSource(elementToUse, skynetElementInHTML[j]);
            }
        }
    }

    /**
     * @static
     * @param {* instanceof StandardElement} classtoPush
     * @memberof SkyPerfPortal
     */
    static classToPush(classtoPush) {
        if (classtoPush instanceof StandardElement) {
            elementsToUse.push(classtoPush);
        } else {
            console.warn(classtoPush, "seems to not extends StandardElement");
        }
    }

    /**
     * Generate the portal URL for an ElementId again
     * @static
     * @param {string} elementId
     * @param {* instanceof StandardElement} typeOfElement
     * @param {boolean} [eleminatePortalChosen=false]
     * @memberof SkyPerfPortal
     */
    static generateAgain(elementId, typeOfElement, eleminatePortalChosen = false) {
        let element = document.getElementById(elementId);
        if (element !== null) {
            let portalUsed = this.skyPerfPortal.extractPortalFromURL(typeOfElement, element);
            if (eleminatePortalChosen === true) {
                blacklist.addToBlacklist(portalUsed);
            }
            this.skyPerfPortal.prepareSource(typeOfElement, element);
        }
    }

    /**
     * Extract the Portal URL from a given element 
     * @param {* instanceof StandardElement} typeOfElement
     * @param {Element} element
     */
    extractPortalFromURL(typeOfElement, element) {
        if (typeOfElement.getType() === Type.Parent) {
            let skynetPortalElementLink = element.getAttribute(typeOfElement.getSPPToAttribute());
            console.debug("Truncate", skynetPortalElementLink);
            return skynetPortalElementLink.slice(0, skynetPortalElementLink.lastIndexOf("/"));
        }
    }

    /**
     * Extract on own element or on child elements
     * skynet hash to deal with it
     * and execute expression before reloading element
     * @param {* instanceof StandardElement} element
     * @param {Element} skynetElement
     */
    prepareSource(element, skynetElement) {
        if (element.getType() === Type.Parent) {
            let skynetElementLink = skynetElement.getAttribute(element.getSPPClassLink());
            if (skynetElementLink !== null) {
                element.getExecuteBeforeReloadingExpression(skynetElement);
                this.checkPortal(element, skynetElement, skynetElementLink);
            }
        } else if (element.getType() === Type.Child) {
            for (let i = 0; i < skynetElement.children.length; i++) {
                if (element.getCompatibleTag().includes(skynetElement.children[i].tagName)) {
                    let skynetElementLink = skynetElement.children[i].getAttribute(element.getSPPClassLink());
                    if (skynetElementLink !== null) {
                        element.getExecuteBeforeReloadingExpression(skynetElement);
                        this.checkPortal(element, skynetElement.children[i], skynetElementLink);
                    }
                }
            }
        }
    }

    /**
     * Change URL 
     * and Execute expression after changing source
     * @param {* instanceof StandardElement} element
     * @param {Element} skynetElement
     * @param {string} skynet_portal_url
     * @param {string} skynetElementLink
     */
    updateSource(element, skynetElement, skynet_portal_url, skynetElementLink) {
        skynetElement.setAttribute(element.getSPPToAttribute(), skynet_portal_url + "/" + skynetElementLink);
        element.getExecuteAfterReloadingExpression(skynetElement);
    }

    /**
     * Have the portal URL from siastats, the first one who success is chosen
     * if it's not in blacklist
     * /!\ It's an asynchronous function that lose the context
     * @param {* instanceof StandardElement} element
     * @param {Element} skynetElement
     * @param {string} skynetLink
     */
    checkPortal(element, skynetElement, skynetLink) {
        fetch('https://siastats.info/dbs/skynet_current.json')
            .then(response => response.json())
            .then(data => {

                let notIntialialized = true;

                for (let i = 0; i < data.length; i++) {
                    let xhr = new XMLHttpRequest();

                    let startTime = (new Date()).getTime();

                    xhr.open('HEAD', data[i]["link"] + "/" + skynetLink);
                    xhr.timeout = timeout; // in milliseconds
                    xhr.onload = function () {
                        console.debug(data[i]["link"], this.readyState, xhr.status);
                        if (this.readyState === 4 && xhr.status === 200) {

                            let endTime = (new Date()).getTime();
                            console.debug({
                                "url": data[i]["link"],
                                "time": endTime - startTime
                            });
                            if (notIntialialized === true && !blacklist.includePortal(data[i]["link"])) {
                                notIntialialized = false;
                                console.debug("This portal has been chosen", data[i]["link"]);
                                (new SkyPerfPortal).updateSource(element, skynetElement, data[i]["link"], skynetLink);
                            }

                        }
                    };
                    xhr.send();
                }
            });
    }
}